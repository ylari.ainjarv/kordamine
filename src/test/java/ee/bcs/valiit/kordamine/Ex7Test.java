package ee.bcs.valiit.kordamine;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

public class Ex7Test {

    String[] personalCode;

    @Before
    public void setUp() {
        String[] personalCode = {
                "37212210230",
                "37212210231"
        };
        this.personalCode = personalCode;
    }

    @Test
    public void testCheckNumber() {
        assertTrue(Ex7.isCheckNumberCorrect(new BigInteger(personalCode[0])));
        assertFalse(Ex7.isCheckNumberCorrect(new BigInteger(personalCode[1])));
    }

    @Test
    public void testYear() {
        int year = Ex7.deriveBirthYear(personalCode[0]);
        assertEquals(year, 1972);
        assertNotEquals(year, 1970);
    }

}
