package ee.bcs.valiit.kordamine.getset;

public class Car {

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    private int speed;

    public void setSpeed(int speed, String role) throws Exception {
        if (role.equals("autojuht")) {
            this.speed = speed;
        } else {
            throw new Exception("Sul pole lubatud kiirust muuta!");
        }
    }

}
