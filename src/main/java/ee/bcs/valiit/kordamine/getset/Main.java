package ee.bcs.valiit.kordamine.getset;

public class Main {

    public static void main(String[] args) {
        Car sapakas = new Car();
        // sapakas.speed = 120;
        // sapakas.setSpeed(120);
        try {
            sapakas.setSpeed(120, "autojuht");
            System.out.println("Kiirus: " + sapakas.getSpeed());
            sapakas.setSpeed(210, "kõrvalsõitja");
            System.out.println("Kiirus: " + sapakas.getSpeed());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        sapakas.setSpeed(210);
        System.out.println("Kiirus: " + sapakas.getSpeed() + " :)");
    }

}
