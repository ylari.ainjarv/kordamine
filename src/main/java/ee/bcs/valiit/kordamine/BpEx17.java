package ee.bcs.valiit.kordamine;

public class BpEx17 {

    public static void main(String[] args) {
        StringBuilder resultaat = new StringBuilder();
        for (int i = 0; i < args.length; i++) {
            switch (args[i]) {
                case "0":
                    resultaat.append("null");
                    break;
                case "1":
                    resultaat.append("üks");
                    break;
                case "2":
                    resultaat.append("kaks");
                    break;
                default:
                    break;
            }
            if (args.length > i + 1) {
                resultaat.append(", ");
            }
        }
        System.out.println(resultaat);
    }

}
