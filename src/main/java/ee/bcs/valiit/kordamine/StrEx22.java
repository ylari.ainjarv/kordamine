package ee.bcs.valiit.kordamine;

import java.util.Scanner;

public class StrEx22 {

    public static void main(String[] args) {
        String str = "Rida: See on esimene rida. Rida: See on teine rida. Rida: See on kolmas rida.";

        // 1: (õige)
        Scanner scanner = new Scanner(str);
        while (scanner.useDelimiter("Rida: ").hasNext()) {
            System.out.println(scanner.next());
        }

        System.out.println();

        // 2: (vale, aga töötab ka)
        String[] resultaat2 = str.split("Rida: ");
        for (String element: resultaat2) {
            System.out.println(element);
        }

        // Näide sõnade arvu leidmiseks tekstist
        String[] sonad = str.split(" ");
        for (String sona: sonad) {
            System.out.println(sona);
        }
        System.out.println("Tekstis oli " + sonad.length + " sõna");
    }

}
