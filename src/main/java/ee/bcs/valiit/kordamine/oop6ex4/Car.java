package ee.bcs.valiit.kordamine.oop6ex4;

import lombok.Data;

@Data
public abstract class Car implements Movable {

    private String color;
    private int speed;
    private int seatCount;

    public Car() {}

    public Car(String color, int speed) {
        this.color = color;
        this.speed = speed;
    }

    public abstract void drive();


}
