package ee.bcs.valiit.kordamine.oop6ex4;

import lombok.Data;

@Data
public abstract class Bike implements Movable {

    String color;
    int speed;

    public abstract void drive();

}
