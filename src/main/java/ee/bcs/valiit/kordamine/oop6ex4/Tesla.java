package ee.bcs.valiit.kordamine.oop6ex4;

public class Tesla extends Car {

    public Tesla() {
        super();
    }

    public Tesla(String color, int speed) {
        super(color, speed);
        setSeatCount(5);
    }

    @Override
    public void drive() {
        // kuidas see Tesla sõidab
    }
}
