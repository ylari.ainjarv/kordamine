package ee.bcs.valiit.kordamine.oop6ex4;

public interface Movable {

    String getColor();
    void setColor(String color);

    int getSpeed();
    void setSpeed(int speed);

    void drive();

}
