package ee.bcs.valiit.kordamine.oop6ex4;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Tesla tesla = new Tesla();
        tesla.setColor("red");
        tesla.setSpeed(120);
        tesla.setSeatCount(5);
        Scott scott = new Scott();
        scott.setColor("blue");
        scott.setSpeed(25);
        List<Movable> movables = new ArrayList<>();
        movables.add(tesla);
        movables.add(scott);
        for (Movable movable : movables) {
            if (movable instanceof Car) {
                System.out.println("Auto on " + movable.getColor());
            } else if (movable instanceof Bike) {
                System.out.println("Ratas on " + movable.getColor());
            }
        }
    }

}
