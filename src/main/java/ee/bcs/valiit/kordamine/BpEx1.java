package ee.bcs.valiit.kordamine;

import java.math.BigInteger;

public class BpEx1 {

    static BigInteger suurNumber = new BigInteger("8784739872394857293485729384752934752938457239857293845723948");

    public static void main(String[] args) {
          int suurInt = 1000000000;
        long suurLong = 1000000000000000000L;

        System.out.println(suurNumber.add(new BigInteger("394856703498560734958763045896730489576309485763094587603945870638945760380763847305")));
    }
}
