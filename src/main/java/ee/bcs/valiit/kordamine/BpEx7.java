package ee.bcs.valiit.kordamine;

public class BpEx7 {

    public static void main(String[] args) {
        int[] massiiv;
        massiiv = new int[5];
        massiiv[0] = 1;
        massiiv[1] = 2;
        massiiv[2] = 3;
        massiiv[3] = 4;
        massiiv[4] = 5;
        System.out.println(massiiv[0]);
        System.out.println(massiiv[2]);
        System.out.println(massiiv[massiiv.length - 1]);
    }

}
