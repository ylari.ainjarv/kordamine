package ee.bcs.valiit.kordamine;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CollEx22 {

    public static void main(String[] args) {
        Map<String, String[]> map = new HashMap<>();
        String[] estCities = {"Tallinn", "Tartu", "Valga", "Võru"};
        map.put("Estonia", estCities);
        String[] sweCities = {"Stockholm", "Uppsala", "Lund", "Köping"};
        map.put("Sweden", sweCities);

        // Variant 1
        for (Map.Entry<String, String[]> country : map.entrySet()) {
            System.out.println("Country: " + country.getKey());
            System.out.println("Cities:");
            for (String city : country.getValue()) {
                System.out.println("\t" + city);
            }
        }

        // Variant 2
        map.forEach((k, v) -> {
            System.out.println("Country: " + k);
            System.out.println("Cities:");
            Arrays.stream(v).forEach(c -> System.out.println("\t" + c));
/*
            for (String city: v) {
                System.out.println("\t" + city);
            }
*/
        });

        // Enesepiinamine
        Object[] keys = map.keySet().toArray();
        for (int i = 0; i < keys.length; i++) {
            if (keys[i] instanceof String) {
                System.out.println("Ongi teksti tüüp");
            }
            String key = (String) keys[i];
            System.out.println("Country: " + key);
            System.out.println("Cities:");
            String[] values = map.get(key);
            for (int j = 0; j < map.get(key).length; j++) {
                System.out.println("\t" + values[j]);
            }
        }
    }

}
