package ee.bcs.valiit.kordamine;

public class StrEx2 {

    public static void main(String[] args) {
        String bookTitle = "Rehepapp";

        // 1:
        String resultaat = "Raamatu \"" + bookTitle + "\" autor on Andrus Kivirähk";

        // 2:
        StringBuilder resultaat2 = new StringBuilder();
        resultaat2.append("Raamatu ");
        resultaat2.append('"');
        resultaat2.append(bookTitle);
        resultaat2.append('"');
        resultaat2.append(" autor on Andrus Kivirähk");

        // 3:
        String resultaat3 = String.format("Raamatu %c%s%c autor on Andrus Kivirähk", '"', bookTitle, '"');

        System.out.println("1: " + resultaat);
        System.out.println("2: " + resultaat2);
        System.out.println("3: " + resultaat3);
    }

}
